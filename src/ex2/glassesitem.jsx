import React, { Component } from 'react'

export default class Glassesitem extends Component {
    
    render() {
        const {url}=this.props.arrProduct;
        return (
            <div onClick={()=>this.props.setSelectedGlasses(this.props.arrProduct)} className="card p2 mx-2">
                <img src={url} alt="hinhsp" />
            </div>
        )
    }
}
