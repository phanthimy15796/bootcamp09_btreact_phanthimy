import React, { Component } from "react";
import Glassesitem from "./glassesitem";
export default class Glasseslist extends Component {
  renderGlasses = () => {
    return this.props.arrProduct.map((item) => {
      return <Glassesitem key={item.id} arrProduct={item} setSelectedGlasses={this.props.setSelectedGlasses}/>;
    });
  };
  render() {
    return (
      <div className="container ">
        <div className="row">
          <div className="bg-white p-3 w-100 d-flex">
            {this.renderGlasses()}
          </div>
        </div>
      </div>
    );
  }
}
