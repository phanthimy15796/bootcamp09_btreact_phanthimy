import React, { Component } from "react";
import "./detail.css";
import model from "./glassesImage/model.jpg";
import glass from "./glassesImage/v2.png";
export default class Detail extends Component {
  renderDetail = () => {
    if (this.props.selectedGlasses) {
        const {url,name,desc}=this.props.selectedGlasses;
      return (
        <>
          <img src={url} alt="" className="img2" />
          <div className="text text-center">
            <h5>{name}</h5>
            <p>
              {desc}
            </p>
          </div>
        </>
      );
    }
  };
  render() {
    return (
      <div className="container">
        <div className="img m-auto" style={{ width: 300 }}>
          <img src={model} alt="" className="img-fluid" />
          {this.renderDetail()}
        </div>
      </div>
    );
  }
}
