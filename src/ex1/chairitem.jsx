import React, { Component } from 'react'

export default class Chairitem extends Component {
    
   
    render() {
        const {SoGhe,TenGhe,TrangThai} =this.props.data;
        let isCheck = this.props.setStatus(SoGhe);
        console.log(isCheck);
        return (
            <>
            {TrangThai ?<button className=" btn btn-danger" disabled style={{width:65}} >{TenGhe}</button>:

           isCheck?
             <button  onClick={()=>this.props.bookChair(this.props.data)}  className=" btn btn-success" style={{width:65}} >{TenGhe}</button>:
             <button  onClick={()=>this.props.bookChair(this.props.data)}  className=" btn btn-secondary" style={{width:65}} >{TenGhe}</button>
             }
        
            </>
        )
    }
}
