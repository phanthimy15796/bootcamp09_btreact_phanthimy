import React, { Component } from "react";

export default class Bookedchairlist extends Component {
  renderBookedChair = () => {
    return this.props.Chairs.map((item) => {
      const { TenGhe, SoGhe, Gia } = item;
      return (
        <p>
          Chair : {TenGhe} ${Gia} <span>[Cancel]</span>
        </p>
      );
    });
  };
  render() {
    return (
      <div>
        <h3>Booked Chair List</h3>
        <div>{this.renderBookedChair()}</div>
      </div>
    );
  }
}
