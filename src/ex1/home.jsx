import React, { Component } from "react";
import Chairlist from "./chairlist";
import Bookedchairlist from "./bookedchairlist";
export default class Home extends Component {
  data = [
    { SoGhe: 1, TenGhe: "số 1 ", Gia: 100, TrangThai: false },
    { SoGhe: 2, TenGhe: "số 2 ", Gia: 100, TrangThai: false },
    { SoGhe: 3, TenGhe: "số 3", Gia: 100, TrangThai: false },
    { SoGhe: 4, TenGhe: "số 4 ", Gia: 100, TrangThai: true },
    { SoGhe: 5, TenGhe: "số 5 ", Gia: 100, TrangThai: false },
    { SoGhe: 6, TenGhe: "số 6 ", Gia: 100, TrangThai: false },
    { SoGhe: 7, TenGhe: "số 7 ", Gia: 100, TrangThai: false },
    { SoGhe: 8, TenGhe: "số 7", Gia: 100, TrangThai: false },
    { SoGhe: 9, TenGhe: "số 9 ", Gia: 100, TrangThai: true },
    { SoGhe: 10, TenGhe: "số 10 ", Gia: 100, TrangThai: false },
    { SoGhe: 11, TenGhe: "số 11 ", Gia: 100, TrangThai: false },
    { SoGhe: 12, TenGhe: "số 12 ", Gia: 100, TrangThai: false },
    { SoGhe: 13, TenGhe: "số 13 ", Gia: 100, TrangThai: false },
    { SoGhe: 14, TenGhe: "số 14 ", Gia: 100, TrangThai: false },
    { SoGhe: 15, TenGhe: "số 15 ", Gia: 100, TrangThai: false },
    { SoGhe: 16, TenGhe: "số 16 ", Gia: 100, TrangThai: false },
    { SoGhe: 17, TenGhe: "số 17 ", Gia: 100, TrangThai: false },
    { SoGhe: 18, TenGhe: "số 18 ", Gia: 100, TrangThai: false },
    { SoGhe: 19, TenGhe: "số 19 ", Gia: 100, TrangThai: false },
    { SoGhe: 20, TenGhe: "số 20 ", Gia: 100, TrangThai: true },
    { SoGhe: 21, TenGhe: "số 21 ", Gia: 100, TrangThai: false },
    { SoGhe: 22, TenGhe: "số 22 ", Gia: 100, TrangThai: false },
    { SoGhe: 23, TenGhe: "số 23 ", Gia: 100, TrangThai: false },
    { SoGhe: 24, TenGhe: "số 24 ", Gia: 100, TrangThai: false },
    { SoGhe: 25, TenGhe: "số 25 ", Gia: 100, TrangThai: false },
    { SoGhe: 26, TenGhe: "số 26 ", Gia: 100, TrangThai: false },
    { SoGhe: 27, TenGhe: "số 27 ", Gia: 100, TrangThai: false },
    { SoGhe: 28, TenGhe: "số 28 ", Gia: 100, TrangThai: false },
    { SoGhe: 29, TenGhe: "số 29 ", Gia: 100, TrangThai: false },
    { SoGhe: 30, TenGhe: "số 30 ", Gia: 100, TrangThai: true },
    { SoGhe: 31, TenGhe: "số 31 ", Gia: 100, TrangThai: false },
    { SoGhe: 32, TenGhe: "số 32 ", Gia: 100, TrangThai: false },
    { SoGhe: 33, TenGhe: "số 33 ", Gia: 100, TrangThai: false },
    { SoGhe: 34, TenGhe: "số 34 ", Gia: 100, TrangThai: false },
    { SoGhe: 35, TenGhe: "số 35 ", Gia: 100, TrangThai: false },
    { SoGhe: 36, TenGhe: "số 36 ", Gia: 100, TrangThai: false },
  ];
 state={
     Chairs:[],
     isBooking:[],
 };
 bookChair=(val)=>{
     const cloneChairs=[...this.state.Chairs];
     const cloneBookChair = [...this.state.isBooking]
     const foundIndex = cloneChairs.findIndex((item)=>{
         return item.SoGhe === val.SoGhe;
     });
     if(foundIndex === -1){
        cloneChairs.push(val);
        cloneBookChair.push(val.SoGhe);
     }else{
        cloneChairs.splice(foundIndex,1);
        cloneBookChair.splice(foundIndex,1);
     }
     this.setState({
         Chairs:cloneChairs,
         isBooking:cloneBookChair,
     });
 };
 setStatus=(id)=>{
   const foundIndex = this.state.isBooking.findIndex((item)=>{
     return item === id;
   });
   if(foundIndex===-1){
     return false;
   }else{
     return true;
   }
 };
  render() {
    return (
      <div className="container" style={{ maxWidth: 700 }}>
        <h1 className="text-center text-warning"> BOOK TICKET BUS</h1>
        <div className="row">
          <div className="col-md-6">
            <Chairlist data={this.data} bookChair={this.bookChair} selectedChair={this.selectedChair} setStatus={this.setStatus}/>
          </div>
          <div className="col-md-6">
            <Bookedchairlist Chairs={this.state.Chairs}/>
          </div>
        </div>
      </div>
    );
  }
}
