import React, { Component } from "react";
import Chairitem from "./chairitem";
export default class Chairlist extends Component {
  renderChair = () => {
    return this.props.data.map((item) => {
      return (
        <div className="col-md-3 mb-4">
          <Chairitem key={item.SoGhe} setStatus={this.props.setStatus} data={item} bookChair={this.props.bookChair} selectedChair={this.props.selectedChair}/>
        </div>
      );
    });
  };
  render() {
    return (
      <div>
        <button className="btn btn-secondary d-block mb-3 w-100 " disabled>
          Tai xe
        </button>
        <div className="row">
            {this.renderChair()}
        </div>
      </div>
    );
  }
}
